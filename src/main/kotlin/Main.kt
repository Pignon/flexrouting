import com.google.ortools.Loader
import com.google.ortools.constraintsolver.*
import com.google.ortools.constraintsolver.main.defaultRoutingSearchParameters

fun printSolution(
    routing: RoutingModel, manager: RoutingIndexManager, solution: Assignment
): MutableList<Int> {
    // Solution cost.
    println("Objective: " + solution.objectiveValue() + "miles")
    var routeDistance: Long = 0
    var route = ""
    var index = routing.start(0)
    val res = mutableListOf<Int>()
    while (!routing.isEnd(index)) {
        route += manager.indexToNode(index).toString() + " -> "
        res.add(manager.indexToNode(index))
        val previousIndex = index
        index = solution.value(routing.nextVar(index))
        routeDistance += routing.getArcCostForVehicle(previousIndex, index, 0)
    }
    route += manager.indexToNode(routing.end(0))
    println(route)
    println("Route distance: " + routeDistance + "miles")
    return res
}


fun main() {
    //Linking librairies
    Loader.loadNativeLibraries()
    val data = DataModel()

    //Creation of the manager
    val manager = RoutingIndexManager(data.matrix.size, data.numV, data.depot)
    val routing = RoutingModel(manager)

    println(data.matrix.first())

    //function that take 2 index and return the distance between those two indexes
    val transitCallbackIndex = routing.registerTransitCallback { fromIndex: Long, toIndex: Long ->
        // Convert from routing variable Index to user NodeIndex.
        val fromNode = manager.indexToNode(fromIndex)
        val toNode = manager.indexToNode(toIndex)
        (data.matrix[fromNode][toNode] * 100000).toLong()
    }

    //Give the routing algorithm, the evaluation methode
    routing.setArcCostEvaluatorOfAllVehicles(transitCallbackIndex)

    //Establish how to evaluate arcs
    val searchParameters: RoutingSearchParameters = defaultRoutingSearchParameters()
        .toBuilder()
        .setFirstSolutionStrategy(FirstSolutionStrategy.Value.PATH_CHEAPEST_ARC)
        .build()

    //Solve with params
    val solution = routing.solveWithParameters(searchParameters)
    val listSolution = printSolution(routing, manager, solution)

    val parser = Parser()
    val jsonSolution = listSolution
        .map {
            if(it==0){
                Parser.GeoPoint(
                    parser.jsonData.CoordMagasins.first().name,
                    parser.jsonData.CoordMagasins.first().x,
                    parser.jsonData.CoordMagasins.first().y,
                    0
                )
            }else{
                parser.jsonData.Coord[it-1]
            }
        }

    parser.save(jsonSolution)
}