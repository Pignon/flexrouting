import kotlinx.serialization.*
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.encodeToJsonElement
import java.io.File
import java.io.FileWriter
import java.io.PrintWriter


class Parser {

    @Serializable
    data class Model(val Coord: List<GeoPoint>, val CoordMagasins: List<GeoPointMagasin>)

    @Serializable
    data class GeoPoint(val name : String, val x:Double, val y:Double, val mag:Int)

    @Serializable
    data class GeoPointMagasin(val name : String, val x:Double, val y:Double)

    fun save(jsonSolution: List<GeoPoint>) {
        val model = Model(jsonSolution, emptyList())
        File("results/path.json").createNewFile()
        PrintWriter(FileWriter("results/path.json")).use {
            it.write(Json.encodeToString(model))
        }
        Json.encodeToJsonElement(model)
    }

    val text = File("src/main/resources/definition.json").readLines().reduce{x,y->x+y}
    val jsonData : Model = Json.decodeFromString(text)
}