import kotlin.math.pow
import kotlin.math.sqrt
import kotlin.random.Random

/**
 * Gather or create datas
 */
class DataModel {

    /**
     * @return Distance between two points
     */
    fun euclidianDist(from: Pair<Double, Double>, to: Pair<Double, Double>): Double {
        println(from)
        println(to)
        println((to.first - from.first)*(to.first - from.first))
        println((to.second - from.second)*(to.second - from.second))
        println((to.first - from.first)*(to.first - from.first) + (to.second - from.second)*(to.second - from.second))
        println()
        return (to.first - from.first)*(to.first - from.first) + (to.second - from.second)*(to.second - from.second)
    }


    //Recuperation des données
    val parser = Parser()
    val deliveries = parser.jsonData.Coord.map {
        Pair(
            it.x,
            it.y
        )
    }.toMutableList()
    val matrix = mutableListOf<ArrayList<Double>>()
    val numV = 1
    val depot = 0

    init {
        deliveries.add(
            0, Pair(
                parser.jsonData.CoordMagasins.first().x,
                parser.jsonData.CoordMagasins.first().y,
            )
        )
        deliveries.forEach { from ->
            val distances = arrayListOf<Double>()
            deliveries.forEach { to ->
                distances.add(euclidianDist(from, to))
            }
            matrix.add(distances)
        }
    }
}