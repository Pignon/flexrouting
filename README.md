# FlexRouting

Given a starting point and a list of points to traverse, returns the shortest path to traverse all the points

## Installation

```
gradle build
```

We recommend to open the whole project with intellij idea

